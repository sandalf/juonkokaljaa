package com.example.sakus.juonkokaljaa;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    ArrayList<String> answers = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        answers.add("Totta kai");
        answers.add("Tietenkin");
        answers.add("Miksikäs ei");
        answers.add("Kyllä");
        answers.add("Ihan hyvä idea");
        answers.add("Kannatan");
        answers.add("Voi voi ei tänään");
        answers.add("Säästele KMP:lle");
        answers.add("Kaljaa? Tähän aikaan");
        answers.add("Ei kyllä nyt maistu");
        answers.add("Suojakännit? Hyvä idea");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final TextView button = findViewById(R.id.button);
        final TextView answer = findViewById(R.id.answer);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (answer.getVisibility() == View.VISIBLE) {
                    answer.setVisibility(View.INVISIBLE);
                } else {
                    answer.setText(answers.get((int) (Math.random() * answers.size())));
                    answer.setVisibility(View.VISIBLE);
                }
        }});
        TextView button2 = findViewById(R.id.button2);
        final TextView answer2 = findViewById(R.id.answer2);
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (answer2.getVisibility() == View.VISIBLE) {
                    answer2.setVisibility(View.INVISIBLE);
                } else {
                    answer2.setVisibility(View.VISIBLE);
                }
            }
        });
    }
}
